function out = my_function(a, b)
if nargin < 2
  b = 10;
end %if
if a < 44
  out = a*44 + b;
elseif a > 44
  out = a/44 * b;
else
  out = 44 - b;
end
end

